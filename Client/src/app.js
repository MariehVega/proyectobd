/**
 * GLOBAL VARS
 */
lastTime = Date.now();
cameras = {
    default: null,
    current: null
};
canvas = {
    element: null,
    container: null
}
labels = {}
cameraControl = null;
scene = null;
renderer = null;
brush =  null;

var mouseRayCaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();

var collidableList = [];

/**
 * Function to start program running a
 * WebGL Application trouhg ThreeJS
 */
let webGLStart = () => {
    initScene();
    window.onresize = onWindowResize;
    window.onmousemove = onMouseMove;
    lastTime = Date.now();
    animateScene();
};

function onMouseMove(event) {
    
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;

}

document.onclick = (e) => {
console.log("Estoy clickeando");
brushAction();
}

function brushAction()
{
	if (brush.mode == "add")
	{
		var cube = new THREE.Mesh( new THREE.BoxGeometry(40,40,40), new THREE.MeshBasicMaterial({ color: Utilities.randomHexColor(), wireframe: false }) );
        cube.position.x = brush.position.x;
        cube.position.y = brush.position.y;
        cube.position.z = brush.position.z;
        cube.collidable = new Collidable (cube,20);
        scene.add(cube);
        collidableList.push(cube);
	}
}

function initScene() {

    //Selección de los elementos del DOM.
    canvas.container = document.querySelector("#app");
    canvas.element = canvas.container.querySelector("#appCanvas");

    //Creación de la escena
    scene = new THREE.Scene();

    //Creación del render
    renderer = new THREE.WebGLRenderer({ canvas: canvas.element });
    renderer.setSize(canvas.container.clientWidth, canvas.container.clientHeight);
    renderer.setClearColor(0x000000, 1);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    canvas.container.appendChild(renderer.domElement);

    //Creación de las cámaras
    cameras.default = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 0.1, 10000);
    cameras.default.position.set(400, 400, -400);
    cameras.default.lookAt(new THREE.Vector3(0, 0, 0));
    cameras.current = cameras.default;
    cameraControl = new THREE.OrbitControls(cameras.current, renderer.domElement);

    //Creción de la luz
    luzAmbiente = new THREE.AmbientLight(0xb5b5b5);
    scene.add(luzAmbiente);

    //Monitor FPS
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = stats.domElement.style.left = '20px';
    stats.domElement.style.zIndex = '100';
    document.body.appendChild(stats.domElement);

    //Creción de la base (suelo)
    let plane = new THREE.Mesh(
        new THREE.PlaneGeometry(400, 400, 10, 10),
        new THREE.MeshBasicMaterial({ color: 0x0000ff, wireframe: true })
    );
    plane.rotation.x =- Math.PI / 2;
    plane.base = true; //Para saber si el cubo está en la base o no
    scene.add(plane);
    collidableList.push(plane);

    //Creación de la brocha
    brush = new THREE.Mesh( new THREE.BoxGeometry(40,40,40), new THREE.MeshBasicMaterial({ color: 0xffffff, wireframe: true }) );	
	brush.ignore = true;    // ignorado por el raycaster
	brush.visible = false;  
	brush.mode = "add";
	scene.add( brush );

    initObjects();
}

function initObjects() {

}

/**
 * Function to render application over
 * and over.
 */
function animateScene() {
    requestAnimationFrame(animateScene);
    renderer.render(scene, cameras.current);
    updateScene();
}

/**
 * Function to evaluate logic over and
 * over again.
 */
function updateScene() {

    lastTime = Date.now();

    //Updating camera view by control inputs
    cameraControl.update();

    //Updating FPS monitor
    stats.update();

    // update the picking ray with the camera and mouse position
    mouseRayCaster.setFromCamera(mouse, cameras.default);
    var intersectionList = [];
	intersectionList = mouseRayCaster.intersectObjects( scene.children );
	var result = null;
	for ( var i = 0; i < intersectionList.length; i++ ) 
	{
		if ( (result == null) && (intersectionList[i].object instanceof THREE.Mesh) && !(intersectionList[i].object.ignore) ) 
            result = intersectionList[i];
    }
    
	//brush will only be visible only in "add" mode
    brush.visible = false;

    if ( result )
	{	
		if ( (brush.mode == "add") && result.object.base ) // place cube on basePlane
		{
			brush.visible = true;
            var intPosition = new THREE.Vector3( Math.floor(result.point.x), 0, Math.floor(result.point.z) );
            brush.position.x = intPosition.x;
            brush.position.y = intPosition.y+20; //Para dejarlos encima del suelo
            brush.position.z = intPosition.z;
        }
        if ( (brush.mode == "add") && !result.object.base ) // place cube on another object
		{
            brush.visible = true;
            brush.position.x = result.object.position.x;
            brush.position.y = result.object.position.y+40; //Para dejarlo encima del otro cubo
            brush.position.z = result.object.position.z;
        }
    }

    for (let i = 1; i < collidableList.length; i++) {
        if (collidableList[i] != null) {
            collidableList[i].collidable.update();
        }
    }

}

function onWindowResize() {
    cameras.current.aspect = window.innerWidth / window.innerHeight;
    cameras.current.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}