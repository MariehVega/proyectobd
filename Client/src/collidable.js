class Collidable {
    constructor(mesh, boundingRadius) {
        this.mesh = mesh;
        this.collidableRadius = boundingRadius;
    }

    collide(normal, callback) {
        let collidableRay = new THREE.Raycaster();
        collidableRay.ray.direction.set(normal.x, normal.y, normal.z);

        let origin = this.mesh.position.clone();
        collidableRay.ray.origin.copy(origin);

        let intersections = collidableRay.intersectObjects(collidableList);

        if (intersections.length > 0) {
            let distance = intersections[0].distance;
            if (distance <= this.collidableRadius-0.5) {
                callback();
            }
        }
    }

    collideLeft() {
        let callback = () => {
            this.mesh.position.x += 0.1;
        }
        this.collide({ x: -1, y: 0, z: 0 }, callback);
    }

    collideRight() {
        let callback = () => {
            this.mesh.position.x -= 0.1;
        }
        this.collide({ x: 1, y: 0, z: 0 }, callback);
    }

    collideUp() {
        let callback = () => {
            this.mesh.position.z += 0.1;
        }
        this.collide({ x: 0, y: 0, z: -1 }, callback);
    }

    collideDown() {
        let callback = () => {
            this.mesh.position.z -= 0.1;
        }
        this.collide({ x: 0, y: 0, z: 1 }, callback);
    }

    collideTop(){
        let callback = () => {
            this.mesh.position.z += 0.1;
        }
        this.collide({ x: 0, y: -1, z: 0 }, callback);
    }


    update() {
        this.collideLeft();
        this.collideRight();
        this.collideUp();
        this.collideDown();
        this.collideTop();
    }
}